########################################################################################
# Build
########################################################################################

FROM node:lts-bullseye AS build
WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install

COPY public public
COPY src src
RUN yarn build


########################################################################################
# Deploy
########################################################################################

FROM nginx:1.21 AS deploy

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /app/build/ /usr/share/nginx/html/
