////////////////////////////////////////////////////////////////////////////////////////
// Config
////////////////////////////////////////////////////////////////////////////////////////

// ProgressBlocks is the max block age in Thorchain blocks since the finalized height
// for which we show observation progress - older just displays the count.
const ProgressBlocks = 1800; // 3h

function setNetwork(network) {
  window.sessionStorage.setItem("network", network);
}

function getNetwork() {
  return window.sessionStorage.getItem("network");
}

function setHeight(height) {
  window.sessionStorage.setItem("height", height);
}

function getHeight() {
  return parseInt(window.sessionStorage.getItem("height"));
}

////////////////////////////////////////////////////////////////////////////////////////
// Exports
////////////////////////////////////////////////////////////////////////////////////////

export { ProgressBlocks, setNetwork, getNetwork, setHeight, getHeight };
